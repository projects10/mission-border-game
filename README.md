# Mission Border - game

Shooter game created by using Java and JavaFX.

## Video link
https://drive.google.com/open?id=1WvAkGMWZLmJtk257XRVHcvDNXHMTlNH3

## Screenshots

![Menu](/screenshots/menu.png)

![Stage1](/screenshots/stage1.png)

![Stage2](/screenshots/stage2.png)

![Stage3](/screenshots/stage3.png)
